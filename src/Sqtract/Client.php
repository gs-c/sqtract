<?php
namespace Sqtract;

use GuzzleHttp\Client as GuzzleClient;

class Client
{
    protected $guzzle;
    
    protected $sqUser = [];
    
    private $contents;
    
    public function __construct($parameters)
    {
        $this->guzzle = new GuzzleClient([
            // Base URI is used with relative requests
            'base_uri' => $parameters['base_uri'] . '/api/',
            // You can set any number of default request options.
            'timeout'  => $parameters['timeout'],
        ]);
        if (array_key_exists('user', $parameters)) {
            $this->sqUser = $parameters['user'];
        }
    }
    
    public function get($url, $withAuth = false)
    {
        
        $result = $this->guzzle->request(
            'GET', 
            $url,
            $withAuth ? $this->sqUser : []
        );
        
        $this->contents = $result->getBody()->getContents();
        
        return $this;
    }
    
    public function json()
    {
        return json_decode($this->contents);
    }
}