<?php
namespace Sqtract;

class Resources extends Client
{
    public function getAll($metrics)
    {
        $metricsString = implode(',', $metrics);
        return $this->get('resources/index?metrics=' . $metricsString)->json();
    }
}