<?php
namespace Sqtract;

class Filename
{
    private $generatedFile;
    
    public function __construct($generatedFile)
    {
        $this->generatedFile = $generatedFile;
    }
    
    public function generate()
    {
        $generatedFile = $this->generatedFile;
        $generatedFilename = $generatedFile['name'];
        preg_match('#\[(.+)\]#', $generatedFilename, $matches);
        $datetimeFormat = $matches[1];
        $now = new \DateTime();
        $formatted = $now->format($datetimeFormat);
        $generatedFilename = preg_replace('#\[(.+)\]#', $formatted, $generatedFilename);
        $generatedFilename .= '.' . $generatedFile['extension'];
        
        return $generatedFilename;
    }
}