<?php
use Sqtract\Resources;
use Sqtract\Filename;

require 'vendor/autoload.php';

$globalConfig = include 'config/sqtract.php';
$localConfig = is_file('config.php') ? include 'config.php': [];

$config = array_merge($globalConfig, $localConfig);
$metrics = $config['metrics'];
$metricsKeys = array_keys($metrics);
$resources = new Resources($config['api']);
$objects = $resources->getAll($metricsKeys);

$metricsOrder = array_flip($metricsKeys);
$filename = new Filename($config['generated_file']);
$generatedFilename = $filename->generate();
$handler = fopen($generatedFilename, 'w');

$headers = array_unshift($metrics, $config['projectname-header']);
fputcsv($handler, $metrics);
foreach ($objects as $object) {
    $lineArray = [];
    $lineArray['project'] = $object->name;
    foreach ($object->msr as $measure) {
        $lineArray[$metricsOrder[$measure->key]] =  $measure->val;
    }
    foreach($metricsOrder as $metricOrder) {
        if (!array_key_exists($metricOrder, $lineArray)) {
            $lineArray[$metricOrder] = 0;
        }
    }
    ksort($lineArray);
    $file = fputcsv($handler, $lineArray);
}
fclose($handler);