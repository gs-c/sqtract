<?php
return [
    'metrics' => [
        // For a list of possible metrics, http://docs.sonarqube.org/display/SONAR/Metric+Definitions
        'sqale_debt_ratio'         => 'Ratio de Dette Technique',
        'coverage'                 => 'Couverture des Tests',
        'blocker_violations'       => 'Violations Bloquantes',
        'duplicated_lines_density' => 'Pourcentage Duplications',
        'comment_lines_density'    => 'Pourcentage Commentaires',
    ],
    'projectname-header' => "Nom du Projet",
    'api' => [
        // Sonarqube server url
        'base_uri' => 'http://sonarqube:9000',
        'timeout'  => 2.0,
        // Sonarqube user
        'user'     => ['admin', 'admin'],
    ],
    'generated_file' => [
        // Name of generated file, will be dated
        'name' => 'SonarqubeExtraction_[Y-m-d_His]',
        'extension' => 'csv',
    ],
];