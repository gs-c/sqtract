#Sqtract
Sonarqube extractor from web api.

### Install
Install PHP 7.

Download the phar file (```sqtract.phar```).

Download the file ```config.php.dist```

### Operations
Rename the file ```config.php.dist``` in ```config.php``` and change the configuration according to your environment.

If in a Linux system, execute ```./sqtract.phar```.

In other environments, execute ```php sqtract.phar``` from command line.

By default the generated csv file is named ```SonarqubeExtraction``` and suffixed by date, time and ```.csv```.